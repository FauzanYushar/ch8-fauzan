import React from "react";
import "./style.css";

const Search = ({ username, email, experience, level }) => {
  return (
    <div className="container">
      <table>
        <tbody>
          <tr>
            <td>
              <p>USERNAME</p>
            </td>
            <td>
              <p>
                {": "}
                {username}
              </p>
            </td>
          </tr>
          <tr>
            <td>
              <p>EMAIL</p>
            </td>
            <td>
              <p>
                {": "}
                {email}
              </p>
            </td>
          </tr>
          <tr>
            <td>
              <p>EXPERIENCE</p>
            </td>
            <td>
              <p>
                {": "}
                {experience}
              </p>
            </td>
          </tr>
          <tr>
            <td className="borderbot">
              <p>LEVEL</p>
            </td>
            <td className="borderbot">
              <p>
                {": "}
                {level}
              </p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Search;
