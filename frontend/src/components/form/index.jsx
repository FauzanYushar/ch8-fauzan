import React from "react";
import "./style.css";

const Form = ({
  username,
  email,
  experience,
  level,
  handleUpdate,
  handleDelete,
}) => {
  return (
    <div className="container">
      <table>
        <tbody>
          <tr>
            <td>
              <p>USERNAME</p>
            </td>
            <td>
              <p>
                {": "}
                {username}
              </p>
            </td>
          </tr>
          <tr>
            <td>
              <p>EMAIL</p>
            </td>
            <td>
              <p>
                {": "}
                {email}
              </p>
            </td>
          </tr>
          <tr>
            <td>
              <p>EXPERIENCE</p>
            </td>
            <td>
              <p>
                {": "}
                {experience}
              </p>
            </td>
          </tr>
          <tr>
            <td>
              <p>LEVEL</p>
            </td>
            <td>
              <p>
                {": "}
                {level}
              </p>
            </td>
          </tr>
          <tr>
            <td colSpan={2} className="borderbot">
              <div className="button">
                <button
                  onClick={() => {
                    handleUpdate(username, email, experience, level);
                  }}
                >
                  UPDATE
                </button>
                <button
                  onClick={() => {
                    handleDelete(username);
                  }}
                >
                  DELETE
                </button>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Form;
