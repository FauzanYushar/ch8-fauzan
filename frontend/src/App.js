import Form from "./components/form";
import Search from "./components/search";
import { useState } from "react";
import "./App.css";

function App() {
  const [username, setUsername] = useState("");
  const [usernamevalue, setUsernameValue] = useState("");
  const [emailvalue, setEmailValue] = useState("");
  const [experiencevalue, setExperienceValue] = useState("");
  const [levelvalue, setLevelValue] = useState("");
  const [searchusername, setSearchUsername] = useState("");
  const [searchemail, setSearchEmail] = useState("");
  const [searchexperience, setSearchExperience] = useState("");
  const [searchlevel, setSearchLevel] = useState("");
  const [data, setData] = useState([]);
  const [dataSearch, setDataSearch] = useState([]);
  const [error, setError] = useState("");
  const [isUpdate, setIsUpdate] = useState(false);
  const [errorSearch, setErrorSearch] = useState("");
  const handleUpdate = (username, email, experience, level) => {
    setIsUpdate(true);
    setUsernameValue(username);
    setEmailValue(email);
    setExperienceValue(experience);
    setLevelValue(level);
    setUsername(username);
  };
  const handleDelete = (username) => {
    const newData = data.filter((row) => row.username !== username);
    setData(newData);
  };
  const handleSearch = () => {
    const isFind = data.find(
      (row) =>
        row.username === searchusername &&
        row.email === searchemail &&
        row.experience === searchexperience &&
        row.level === searchlevel
    );
    if (isFind) {
      setDataSearch([isFind]);
      setSearchUsername("");
      setSearchEmail("");
      setSearchExperience("");
      setSearchLevel("");
    } else {
      setErrorSearch("Data does not exist");
      setTimeout(() => {
        setErrorSearch("");
      }, 1500);
    }
  };
  const handleSubmit = () => {
    if (!isUpdate) {
      const isFind = data.find((row) => row.username === usernamevalue);
      if (!isFind) {
        const newData = [
          ...data,
          {
            username: usernamevalue,
            email: emailvalue,
            experience: experiencevalue,
            level: levelvalue,
          },
        ];
        setData(newData);
        setUsernameValue("");
        setEmailValue("");
        setExperienceValue("");
        setLevelValue("");
      } else {
        setError("Name Already taken");
        setTimeout(() => {
          setError("");
        }, 1500);
      }
    } else {
      if (username === usernamevalue) {
        const dataUpdate = {
          username: usernamevalue,
          email: emailvalue,
          experience: experiencevalue,
          level: levelvalue,
        };
        const newData = data.map((row) =>
          row.username === username ? dataUpdate : row
        );
        setData(newData);
        setIsUpdate(false);
        setUsernameValue("");
        setEmailValue("");
        setExperienceValue("");
        setLevelValue("");
        setUsername("");
      } else {
        const isFind = data.find((row) => row.username === usernamevalue);
        if (!isFind) {
          const dataUpdate = {
            username: usernamevalue,
            email: emailvalue,
            experience: experiencevalue,
            level: levelvalue,
          };
          const newData = data.map((row) =>
            row.username === username ? dataUpdate : row
          );
          setData(newData);
          setIsUpdate(false);
          setUsernameValue("");
          setEmailValue("");
          setExperienceValue("");
          setLevelValue("");
          setUsername("");
        } else {
          setError("Name Already taken");
          setTimeout(() => {
            setError("");
          }, 1500);
        }
      }
    }
  };

  return (
    <div className="main-container">
      <div className="form-container">
        <div className="form">
          <table>
            <tbody>
              <tr>
                <td colSpan={2}>
                  <h2>FORM INPUT & UPDATE</h2>
                </td>
              </tr>
              <tr>
                <td>USERNAME</td>
                <td>
                  {": "}
                  <input
                    value={usernamevalue}
                    onChange={(e) => setUsernameValue(e.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td>EMAIL</td>
                <td>
                  {": "}
                  <input
                    value={emailvalue}
                    onChange={(e) => setEmailValue(e.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td>EXPERIENCE</td>
                <td>
                  {": "}
                  <input
                    value={experiencevalue}
                    onChange={(e) => setExperienceValue(e.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td>LEVEL</td>
                <td>
                  {": "}
                  <input
                    value={levelvalue}
                    onChange={(e) => setLevelValue(e.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td colSpan={2}>
                  {usernamevalue &&
                    emailvalue &&
                    experiencevalue &&
                    levelvalue && (
                      <button onClick={handleSubmit}>SUBMIT</button>
                    )}
                </td>
              </tr>
              <tr>
                <td className="error" colSpan={2}>
                  {error && <div>{error}</div>}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="datacontainer">
          {data.map((row, index) => (
            <Form
              username={row.username}
              email={row.email}
              experience={row.experience}
              level={row.level}
              handleUpdate={handleUpdate}
              handleDelete={handleDelete}
              key={index}
            />
          ))}
        </div>
      </div>
      <div className="search-container">
        <div className="form">
          <table>
            <tbody>
              <tr>
                <td colSpan={2}>
                  <h2>FORM SEARCH</h2>
                </td>
              </tr>
              <tr>
                <td>USERNAME</td>
                <td>
                  {": "}
                  <input
                    value={searchusername}
                    onChange={(e) => setSearchUsername(e.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td>EMAIL</td>
                <td>
                  {": "}
                  <input
                    value={searchemail}
                    onChange={(e) => setSearchEmail(e.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td>EXPERIENCE</td>
                <td>
                  {": "}
                  <input
                    value={searchexperience}
                    onChange={(e) => setSearchExperience(e.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td>LEVEL</td>
                <td>
                  {": "}
                  <input
                    value={searchlevel}
                    onChange={(e) => setSearchLevel(e.target.value)}
                  ></input>
                </td>
              </tr>
              <tr>
                <td colSpan={2}>
                  {searchusername &&
                    searchemail &&
                    searchexperience &&
                    searchlevel && (
                      <button onClick={handleSearch}>SUBMIT</button>
                    )}
                </td>
              </tr>
              <tr>
                <td className="error" colSpan={2}>
                  {errorSearch && <div>{errorSearch}</div>}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="datacontainer">
          {dataSearch.map((row, index) => (
            <Search
              username={row.username}
              email={row.email}
              experience={row.experience}
              level={row.level}
              key={index}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

export default App;
